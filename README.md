# Plymouth theme featuring the new [Linux Mint Cinnamon Logo](http://segfault.linuxmint.com/2016/09/design-team-working-on-spices-website/)

Builds can be found [here](https://gitlab.com/packaging/plymouth-theme-cinnamon-logo/pipelines) and in my [repo](https://repo.morph027.de).

## Preview

Also features a rotating spinner, which isn't shown  :smile:

![](preview.png)
