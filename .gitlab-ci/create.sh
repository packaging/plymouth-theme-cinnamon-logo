#!/bin/sh

create_package() {
TYPE="$1"
fpm -f -a all -s dir -t $TYPE -n "plymouth-theme-${THEME_DIR#*/}" \
--prefix="/usr/share/plymouth/themes" \
--vendor "${VENDOR:-Me}" \
--maintainer "${MAINTAINER:-Myself}" \
--version "$(date +%Y%m%d)~git~${CI_BUILD_REF:0:7}" \
--after-install package_scripts/after-install.sh \
--before-remove package_scripts/before-remove.sh \
--description "${DESCRIPTION:-Plymouth theme}" \
--depends plymouth-label \
-C ${THEME_DIR%%/*} \
${THEME_DIR#*/}
}

which fpm > /dev/null

if [ $? -eq 0 ]; then
  THEME_DIR="package_root/cinnamon-logo"
  create_package deb
else
  echo "fpm not found in PATH"
  exit 1
fi
