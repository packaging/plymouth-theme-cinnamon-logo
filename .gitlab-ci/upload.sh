#!/bin/bash

_upload() {
  curl -fsSL -X POST \
  -F token=$APTLY_CLI_PRIVATE_TOKEN \
  -F ref=master \
  -F "variables[REPO_NAME]=$REPO_NAME" \
  -F "variables[REPO_DIST]=$REPO_DIST" \
  -F "variables[PROJECT]=$CI_PROJECT_ID" \
  -F "variables[STAGE]=package" \
  -F "variables[REF]=${CI_BUILD_TAG:-master}" \
  $APTLY_CLI_URL
}

if [ ! -z $APTLY_CLI_URL ]; then

  VARS=( REPO_NAME REPO_DIST CI_PROJECT_ID )
  . <(curl -fsSL https://gitlab.com/morph027/gitlab-ci-helpers/raw/master/check-vars.sh)
  _upload

fi
